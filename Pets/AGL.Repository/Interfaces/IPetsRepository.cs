﻿using AGL.Models;
using System.Collections.Generic;

namespace AGL.Repository.Interfaces
{
    public interface IPetsRepository
    {
        IList<Owner> GetAllOwners();
    }
}
