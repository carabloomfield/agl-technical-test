﻿using System.Collections.Generic;

namespace AGL.ViewModel
{

    public class PetsByOwnerGenderViewModel
    {
        public string Gender { get; set; }
        public List<string> PetNames { get; set; }
    }

}
