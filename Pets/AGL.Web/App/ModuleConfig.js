﻿var AGLApp = angular.module("AGLApp", []);

AGLApp.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.delete = {};
    $httpProvider.defaults.headers.patch = {};

})