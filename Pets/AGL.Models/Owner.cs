﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Models
{
    public class Owner
    {
        public Owner()
        {
            Pets = new List<Pet>();
        }

        public string Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public IList<Pet> Pets { get; set; }
    }
}
