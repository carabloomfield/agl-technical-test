﻿using System.Collections.Generic;
using AGL.ViewModel;

namespace AGL.Business.Interfaces
{
    public interface IPetsService
    {
        IList<PetsByOwnerGenderViewModel> GetCatNamesByOwnerGender();
    }
}
