﻿AGLApp.controller('PetsController', ["$scope", "$q", "PetsFactory",
    function ($scope, $q, petsFactory) {
        $scope.PetsByOwnerGender = [];

        var populatePetsByOwnerGender = function () {
            var defer = $q.defer();

            petsFactory.PetsByOwnerGender().then(function (data) {
                $scope.PetsByOwnerGender = data;
                defer.resolve(data);

            }, function (data) {
                defer.reject(data);
            });
            return defer.promise;
        }


        populatePetsByOwnerGender();

    }]);