﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AGL.Business.Interfaces;
using log4net;

namespace Pet.WebAPI.Controllers
{
    public class PetsController : ApiController
    {
        private IPetsService _petsService;
        ILog log = LogManager.GetLogger(typeof(PetsController));

        public PetsController(IPetsService petsService)
        {
            _petsService = petsService;
        }

        [HttpGet]
        public HttpResponseMessage CatNamesByOwnerGender()
        {
            try
            {
                var content = _petsService.GetCatNamesByOwnerGender();
                return Request.CreateResponse(HttpStatusCode.OK, content);
            }
            catch(Exception ex)
            {
                log.Error(ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

    }
}
