﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System;
using Moq;
using System.Threading.Tasks;
using System.Net;
using Moq.Protected;
using System.Threading;
using System.Configuration;

namespace AGL.Repository.Test
{

    [TestClass]
    public class PetRepositoryTests
    {
        private const string Uri = "http://agl-developer-test.azurewebsites.net/people.json";

        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            ConfigurationManager.AppSettings["OwnersAPI"] = Uri;
        }

        [TestMethod]
        public void GetAllOwners_EmptyAPIResponse_ReturnsEmptyList()
        {
            Uri requestUri = new Uri(Uri);
            string expectedResponse = "";

            Mock<HttpClientHandler> mockHandler = new Mock<HttpClientHandler>();

            mockHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(message => message.RequestUri == requestUri), ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(expectedResponse) }));
            HttpClient httpClient = new HttpClient(mockHandler.Object);
            
            var petRepo = new PetsRepository(httpClient);

            // Act
            var result = petRepo.GetAllOwners();

            // Assert
            Assert.AreEqual(0, result.Count);

        }

        [TestMethod]
        public void GetAllOwners_APIReturns1Owner_Returns1Owner()
        {
            // Arrange
           var requestUri = new Uri(Uri);
            string expectedResponse = "[{'name':'Bob','gender':'Male','age':23,'pets':[{'name':'Garfield','type':'Cat'},{'name':'Fido','type':'Dog'}]}]";

            Mock<HttpClientHandler> mockHandler = new Mock<HttpClientHandler>();

            mockHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(message => message.RequestUri == requestUri), ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(expectedResponse) }));
            HttpClient httpClient = new HttpClient(mockHandler.Object);

            var petRepo = new PetsRepository(httpClient);

            // Act
            var result = petRepo.GetAllOwners();

            // Assert
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpRequestException))]
        public void GetAllOwners_FailedAPICall_ThrowsExcepion()
        {
            Uri requestUri = new Uri(Uri);
            var expectedResponse = "test exception";

            Mock<HttpClientHandler> mockHandler = new Mock<HttpClientHandler>();

            mockHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(message => message.RequestUri == requestUri), ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent(expectedResponse) }));
            HttpClient httpClient = new HttpClient(mockHandler.Object);

            var petRepo = new PetsRepository(httpClient);

            // Act
            var result = petRepo.GetAllOwners();
          
        }
    }
}
