﻿using AGL.Business.Interfaces;
using AGL.Models;
using AGL.Repository.Interfaces;
using System.Collections.Generic;
using AGL.ViewModel;
using System.Linq;

namespace AGL.Business
{
    public class PetsService : IPetsService
    {
        private IPetsRepository petsRepository;


        public PetsService(IPetsRepository _petsRepository)
        {
            petsRepository = _petsRepository;

        }


        public IList<PetsByOwnerGenderViewModel> GetCatNamesByOwnerGender()
        {
            var owners = petsRepository.GetAllOwners();
            
            var genders =
                owners
                .Where(x => x.Pets.Any(z => z.Type.Equals("cat", System.StringComparison.InvariantCultureIgnoreCase)))
                .Select(y => y.Gender)
                .Distinct().ToList();


            var result = new List<PetsByOwnerGenderViewModel>();
            foreach(var gender in genders)
            {
                var catNames = owners
                    .Where(x => x.Gender.Equals(gender, System.StringComparison.InvariantCultureIgnoreCase))
                    .SelectMany(y => y.Pets.Where(t => t.Type.Equals("cat", System.StringComparison.InvariantCultureIgnoreCase))
                    .Select(u => u.Name))
                    .OrderBy(i => i)
                    .ToList();

                result.Add(new PetsByOwnerGenderViewModel()
                {
                    Gender = gender,
                    PetNames = catNames
                });
            }

            return result;
        }
    }
}
