﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Serialization;
using Unity;
using Pet.WebAPI.Infrastructure;
using AGL.Business;
using AGL.Business.Interfaces;
using System.Net.Http;
using Unity.Lifetime;
using Unity.Injection;
using AGL.Repository;
using AGL.Repository.Interfaces;
using System.Web.Http.Cors;

namespace Pet.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            ResolveContainer(config);
            MapRoutes(config);

            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            //  var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().FirstOrDefault(); // get the first jsonmediatypeformatter and update it
            // jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

        }

        private static void ResolveContainer(HttpConfiguration config)
        {
            var container = new UnityContainer();

            container.RegisterType<IPetsService, PetsService>();
            container.RegisterType<IPetsRepository, PetsRepository>();
            container.RegisterType<HttpClient, HttpClient>(new ContainerControlledLifetimeManager(), new InjectionConstructor());


            config.DependencyResolver = new UnityResolver(container);
        }

        private static void  MapRoutes(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
