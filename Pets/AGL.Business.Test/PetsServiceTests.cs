﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AGL.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using AGL.Repository.Interfaces;

namespace AGL.Business.Test
{
    [TestClass]
    public class PetsServiceTests
    {
        const string MoqJson = "[{\"name\":\"Bob\",\"gender\":\"Male\",\"age\":23,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"},{\"name\":\"Fido\",\"type\":\"Dog\"}]},{\"name\":\"Jennifer\",\"gender\":\"Female\",\"age\":18,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"}]},{\"name\":\"Steve\",\"gender\":\"Male\",\"age\":45,\"pets\":null},{\"name\":\"Fred\",\"gender\":\"Male\",\"age\":40,\"pets\":[{\"name\":\"Tom\",\"type\":\"Cat\"},{\"name\":\"Max\",\"type\":\"Cat\"},{\"name\":\"Sam\",\"type\":\"Dog\"},{\"name\":\"Jim\",\"type\":\"Cat\"}]},{\"name\":\"Samantha\",\"gender\":\"Female\",\"age\":40,\"pets\":[{\"name\":\"Tabby\",\"type\":\"Cat\"}]},{\"name\":\"Alice\",\"gender\":\"Female\",\"age\":64,\"pets\":[{\"name\":\"Simba\",\"type\":\"Cat\"},{\"name\":\"Nemo\",\"type\":\"Fish\"}]}]";
       

        [TestMethod]
        public void GetCatNamesByOwnerGender_CorrectListReturned()
        {
            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            var moqResult = JsonConvert.DeserializeObject<List<Owner>>(MoqJson, settings);

            // Arrange
            Mock<IPetsRepository> mockRepo = new Mock<IPetsRepository>();

            mockRepo.Setup(x => x.GetAllOwners()).Returns(moqResult);

            var petService = new PetsService(mockRepo.Object);

            // Act
            var result = petService.GetCatNamesByOwnerGender();

            // Assert
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(3, result.Where(x => x.Gender.Equals("Female", System.StringComparison.InvariantCultureIgnoreCase)).Select(y => y.PetNames).FirstOrDefault().Count());
            Assert.AreEqual(4, result.Where(x => x.Gender.Equals("Male", System.StringComparison.InvariantCultureIgnoreCase)).Select(y => y.PetNames).FirstOrDefault().Count());

        }


    }
}
