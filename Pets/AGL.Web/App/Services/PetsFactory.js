﻿
AGLApp.factory("PetsFactory", ["$http", "$q",
    function ($http, $q) {

        var petsFactory = {};

        petsFactory.PetsByOwnerGender = function () {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'http://www.agl.webapi/api/pets/CatNamesByOwnerGender ',
                header: {
                    "Content-Type": "application/json"
                }
            }).then(function (result) {
                return defer.resolve(result.data);
            }).catch(function (result) {
                console.log(result);
            });

            return defer.promise;
        }
        

        return petsFactory;
    }]);