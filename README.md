# README #

AGL Technical Test
Version 1

Setup API (WebAPI project) as a website
Update endpoint in PetsFactory.js as configured above

Logging to C:\temp\logs\AGLlog.txt

I would like to have also completed the below but have unfortunately run out of time:
Testing in Angularjs - this is something I haven't done before but will be researching and getting a basic understanding of over the next couple of weeks
Endpoint is hardcoded in Angularjs (PetsFactory.js).  As the endpoint is only used once I didn't see the point but eventually this endpoint (and others) would need to be handled differently
Removed some of the bloat - eg unused header