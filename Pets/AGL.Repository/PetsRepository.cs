﻿using AGL.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Http;
using AGL.Repository.Interfaces;
using System.Configuration;

namespace AGL.Repository
{
    public class PetsRepository : IPetsRepository
    {
        private HttpClient httpClient;
        private string _uri;

        public PetsRepository(HttpClient client)
        {
            httpClient = client;
            _uri = ConfigurationManager.AppSettings["OwnersAPI"];
        }

        public IList<Owner> GetAllOwners()
        {
            var response = httpClient.GetAsync(_uri).Result;

            response.EnsureSuccessStatusCode();

            //if (response.IsSuccessStatusCode)
            //{
            var responseContent = response.Content;

            string responseString = responseContent.ReadAsStringAsync().Result;

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            var owners = JsonConvert.DeserializeObject<List<Owner>>(responseString, settings);

            return owners ?? new List<Owner>();
            //}

            //return new List<Owner>();
        }
    }
}
